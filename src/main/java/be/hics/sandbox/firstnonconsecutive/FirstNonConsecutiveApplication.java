package be.hics.sandbox.firstnonconsecutive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;
import java.util.stream.Collectors;

@SpringBootApplication
public class FirstNonConsecutiveApplication {

    public static void main(String[] args) {
        SpringApplication.run(FirstNonConsecutiveApplication.class, args);
        findFirstNonConsecutive(new int[]{1});
        findFirstNonConsecutive(new int[]{10, 11, 12});
        findFirstNonConsecutive(new int[]{100, 101, 102, 104, 106});
    }

    private static void findFirstNonConsecutive(final int[] array) {
        System.out.print(String.format("[%s] => ", Arrays.stream(array).mapToObj(String::valueOf).collect(Collectors.joining(","))));
        System.out.println(FirstNonConsecutive.find(array));
    }
}
